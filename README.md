---
status
  usable: true
  phase: concept development
  goal: produce sd-card bootable image with mainline U-boot and Kernel
---

# build Custom OrangePI PC/one Boot


build for ORangepi PC inspired from [github/megous orange-pi-5.3](https://github.com/megous/linux/blob/orange-pi-5.3/README.md)

and build from sources: 

1. [arm-trusted-firmware](https://github.com/ARM-software/arm-trusted-firmware)
2. [megous u-boot opi-v2019.07](https://megous.com/git/u-boot/tree/opi-v2019.07)
3. [megous linux orange-pi-5.](https://github.com/megous/linux/tree/orange-pi-5.3)
4. [megous firmware](https://megous.com/git/linux-firmware)
5. [arm-linux-gcc-8.2](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-a/downloads)

## use armbian

download from [armbian](https://www.armbian.com/orange-pi-pc/)

and dd to sd card:

`sudo dd if=Armbian_5.90_Orangepipc_Ubuntu_bionic_next_4.19.57_desktop.img  of=/dev/disk3 bs=4M status=progress`


## Install this output

From `boards/sunxi/READMI.sunxi`:

```
microSD card
------------
Transfer the SPL and the U-Boot FIT image directly to an uSD card:
# dd if=spl/sunxi-spl.bin of=/dev/sdx bs=8k seek=1
# dd if=u-boot.itb of=/dev/sdx bs=8k seek=5
# sync
(replace /dev/sdx with you SD card device file name, which could be
/dev/mmcblk[x] as well).

Alternatively you can use the SPL and the U-Boot FIT image combined into a
single file and transfer that instead:
# dd if=u-boot-sunxi-with-spl.bin of=/dev/sdx bs=8k seek=1

You can partition the microSD card, but leave the first MB unallocated (most
partitioning tools will do this anyway).
```

* uboot.bin (`spl/sunxi-spl.bin`+`uboot.itb` or  `u-boot-sunxi-with-spl.bin`)

install as `dd if=uboot.bin of=/dev/<mydev> bs=8k seek 1 `

or according to [sunxi mainline U-boot wiki](https://linux-sunxi.org/Mainline_U-Boot)

```bash
 dd if=u-boot-sunxi-with-spl.bin of=/dev/sdX bs=1024 seek=8
```
more info on [sdcard booting](https://linux-sunxi.org/Bootable_SD_card#Bootloader)

* `uImage` the kernal packed for u-boot
* `board.dtb` the device tree

## Usage

use armbiam image `Armbian_5.90_Orangepione_Ubuntu_bionic_next_4.19.57.img`

```
dd if=Armbian_5.90_Orangepione_Ubuntu_bionic_next_4.19.57.img of=/dev/<mydev> bs=4M
```

insert into `Orangepi one` device

and boot, execute:


``` bash
sudo rm /boot
curl -O https://opi-one-cam.gioxapp.com/latest/files/full-boot.tar.xz
sudo tar -xf full-boot.tar.xz -C /
sudo  dd if=uboot.bin of=/dev/mmcblk0 bs=1024 seek=8
sudo rm /uboot.bin
sudo reboot
```
