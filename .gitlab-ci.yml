stages:
- clean
- prep
- imageprep
- build
- review
- staging
- production

variables:
  #REBUILD_BUILD_IMAGE: "true"
  #REBUILD_WS_TC: "true"
  #REBUILD_WS_U_BOOT: "true"
  #REBUILD_WS_SRC_LINUX: "true"
  #REBUILD_WS_SRC_FW_LINUX: "true"
  ODAGRUN_IMAGE_LICENSE: MIT
  ODAGRUN_IMAGE_VENDOR: Gioxa Ltd.
  ODAGRUN_IMAGE_TITLE: Custom Build-Image with Linaro GNU-ARM GCC8.2 Toolchain
  ODAGRUN_IMAGE_REFNAME: gnu-arm-gcc82-aarch64
  DISTRO_RELEASE: 7
  DEPLOY_DOMAIN_APP: "gioxapp.com"
  #
  # Prodduction deployment url and https
  DEPLOY_DOMAIN: "opi-one-cam.gioxapp.com"
  DEPLOY_CONFIG_HTTPS: "True"
  #
  #Deployment locations
  DEPLOY_RELEASE_PATH: '["public/"]'
  #
  # Extra links for release page, besides the <*.url> files
  #DEPLOY_hrefs: '[ "website": "http://www.odagrun.com"]'

.linux-clean:
  stage: clean
  image: scratch
  tags:
  - odagrun
  only:
    variables:
    - $REBUILD_WS_SRC_LINUX
  variables:
    ODAGRUN_POD_SIZE: medium
    GIT_CACHE_STRATEGY: push-pull
    WORK_SPACES: |
          - name: opi-linux-5.4-src
            scope: global
            path:
              - linux
            strategy: clean
  script:
     - cd

linux:
  stage: prep
  image: gioxa/base-image-crossimagebuilder
  tags:
  - odagrun
  only:
    variables:
    - $REBUILD_WS_SRC_LINUX
  variables:
    ODAGRUN_POD_SIZE: medium
    WORK_SPACES: |
          - name: opi-linux-5.4
            scope: global
            path:
              - linux
            strategy: push
  script:
     - |
         git config --global user.email $GITLAB_USER_EMAIL
         git config --global user.name $GITLAB_USER_NAME
         git clone --depth=1 --single-branch --branch orange-pi-5.4 https://github.com/megous/linux
        
u-boot:
  stage: prep
  image: gioxa/base-image-crossimagebuilder
  tags:
  - odagrun
  only:
    variables:
    - $REBUILD_WS_U_BOOT
  variables:
    ODAGRUN_POD_SIZE: medium
    GIT_CACHE_STRATEGY: pull
    WORK_SPACES: |
          - name: u-boot-megous-v2019.10
            scope: global
            path:
              - u-boot
            strategy: push
            threshold:
              path: 
                - u-boot/.git/*          
  script:
   - |
       if [[ -d u-boot ]]; then
       cd u-boot
       git pull origin opi-v2019.10
       else 
       git config --global user.email $GITLAB_USER_EMAIL
       git config --global user.name $GITLAB_USER_NAME
       git clone --single-branch --branch opi-v2019.10 https://megous.com/git/u-boot/
       cd u-boot
       git branch
       cat arch/arm/mach-sunxi/Kconfig
       git fsck --full
       fi

gnueabihf-arm-gcc:
  stage: prep
  image: gioxa/base-image-crossimagebuilder
  tags:
  - odagrun
  only:
    variables:
    - $REBUILD_WS_TC
  variables:
    ODAGRUN_POD_SIZE: medium
    GIT_CACHE_STRATEGY: pull
    WORK_SPACES: |
          - name: gnueabihf-arm-gcc
            scope: global
            path:
              - Toolchains
            strategy: push-pull
            threshold:
              path: 
                - Toolchains/*
  script:
   - |
       mkdir -pv Toolchains
       cd Toolchains
       if [[ ! -f gcc-arm-8.2-2019.01-x86_64-arm-linux-gnueabihf.tar.xz ]]; then
       curl -o gcc-arm-8.2-2019.01-x86_64-arm-linux-gnueabihf.tar.xz -L "https://developer.arm.com/-/media/Files/downloads/gnu-a/8.2-2019.01/gcc-arm-8.2-2019.01-x86_64-arm-linux-gnueabihf.tar.xz?revision=c69ea519-a965-4359-84a4-cbd440ff4130&la=en"
       fi
       
   - echo done     

linux-firmware:
  stage: prep
  image: gioxa/base-image-crossimagebuilder
  tags:
  - odagrun
  only:
    variables:
    - $REBUILD_WS_SRC_FW_LINUX
  variables:
    ODAGRUN_POD_SIZE: medium
    GIT_CACHE_STRATEGY: pull
    WORK_SPACES: |
          - name: linux-firmware-src
            scope: global
            path:
              - linux-firmware
            strategy: push-pull
            threshold:
              path: 
                - linux-firmware/.git
  script:
   - |
       git config --global user.email $GITLAB_USER_EMAIL
       git config --global user.name $GITLAB_USER_NAME
       if [[ -d linux-firmware ]]; then
       cd linux-firmware
       git pull origin
       else 
       git clone https://megous.com/git/linux-firmware/
       fi


buildimageprep:
  image: "gioxa/imagebuilder-c${DISTRO_RELEASE}"
  stage: imageprep
  tags:
   - odagrun
  only:
    variables:
      - $REBUILD_BUILD_IMAGE
  variables:
    ODAGRUN_POD_SIZE: xxlarge
    GIT_CACHE_STRATEGY: pull
    WORK_SPACES: |
          - name: gnueabihf-arm-gcc
            scope: global
            strategy: pull
            mandatory: true     
  script:
    - export target="${CI_PROJECT_DIR}/rootfs"
    - export BASE="${CI_PROJECT_DIR}"
    - export OS_CONFIG=make_os.conf
    - mkdir -pv $target
    - make_os
    - ls -la ${target}/opt/Toolchains/arm/bin
    - registry_push --rootfs=$target --ISR --name=buildimage --reference=arm --config=docker_config.yml . 
 
build:
  image: ImageStream/buildimage:arm
  stage: build
  tags:
   - odagrun
  variables:
    BOARD_NAME: opi-one-cam
    ODAGRUN_POD_SIZE: xxlarge
    GIT_CACHE_STRATEGY: push-pull
    WORK_SPACES: |
          - name: opi-linux-5.4
            scope: global
            strategy: pull
            mandatory: true
          - name: u-boot-megous-v2019.10
            scope: global
            strategy: pull
            mandatory: true
          - name: linux-firmware-src
            scope: global
            strategy: pull
            mandatory: true
  script:
  - | 
      export CWD=`pwd`
      export UBOOT_SRC="${CWD}/u-boot"
      export OUT="${CWD}/builds"
      export MYBOARD_ROOT="builds/${BOARD_NAME}"
      export MYBOARD="${CWD}/${MYBOARD_ROOT}"
      export CROSS_COMPILE=arm-linux-gnueabihf-
      export KBUILD_OUTPUT=$OUT/.tmp/uboot-one
  - mkdir -p $MYBOARD public
  - cp -rf boot $MYBOARD
  - >-
        copy
        --from_file=$MYBOARD/boot/overlay/opi-cam-ov5640.in.dts
        --to_file=$MYBOARD/boot/overlay/opi-cam-ov5640.dts 
        --substitute
        --quiet
  - >-
        copy
        --from_file=$MYBOARD/boot/overlay/opi-pn532-i2c.in.dts
        --to_file=$MYBOARD/boot/overlay/opi-pn532-i2c.dts 
        --substitute
        --quiet
  - >-
        copy
        --from_file=$MYBOARD/boot/overlay/opi-spi-dev.in.dts
        --to_file=$MYBOARD/boot/overlay/opi-spi-dev.dts 
        --substitute
        --quiet
  - >-
        copy
        --from_file=$MYBOARD/boot/overlay/opi-one-hdmi.in.dts
        --to_file=$MYBOARD/boot/overlay/opi-one-hdmi.dts 
        --substitute
        --quiet
  - rm -fv $MYBOARD/boot/overlay/*.in.dts
  - echo "${BOARD_NAME}" > ${MYBOARD}/boot/board_name.txt
  - mkdir -p $KBUILD_OUTPUT
  - >-
      copy
      --from_file=configs/u-boot-opi-h3-ext4-defconfig
      --to_file=$UBOOT_SRC/configs/my_defconfig 
      --substitute
      --quiet
  - cd $UBOOT_SRC
  - git apply --binary --reject --recount --ignore-space-change --ignore-whitespace -v ../patches/sunxi-boot-splash.patch
  - git config --global user.email $GITLAB_USER_EMAIL
  - git config --global user.name $GITLAB_USER_NAME
  - git commit -a -m"sunxi-boot-splash.patch"
  - git tag opi-v2019.10.1
  - cd $CWD
  - make -C $UBOOT_SRC my_defconfig
  - >-
      make -C $UBOOT_SRC -j7 > /dev/null
  - cp -fv $KBUILD_OUTPUT/.config public/uboot.config
  - make -C $UBOOT_SRC savedefconfig 
  - cp -v $KBUILD_OUTPUT/defconfig public/u-boot-${BOARD_NAME}-defconfig
  - >-
     cp $KBUILD_OUTPUT/u-boot-sunxi-with-spl.bin $MYBOARD/uboot.bin
  - rm -rf $KBUILD_OUTPUT
  - |
     export ARCH=arm
     export CROSS_COMPILE=arm-linux-gnueabihf-
     export KBUILD_OUTPUT="${OUT}/.tmp/linux-arm"
     export INSTALL_MOD_PATH="${MYBOARD}"
     export INSTALL_HDR_PATH="${MYBOARD}"
  - mkdir -pv $KBUILD_OUTPUT
  - cd linux && git apply -v ${CWD}/patches/enable_symbool_generation_dts.patch
  - cd $CWD
  #- make -C linux -j5 clean
  - >-
       copy
       --from_file=configs/kernel-opi-h3-cam-defconfig
       --to_file=linux/arch/arm/configs/opi-h3-cam_defconfig
       --substitute
       --quiet
  #- export KBUILD_DEFCONFIG=opi_h3_cam_defconfig
  #- cp configs/linux.config $KBUILD_OUTPUT/.config
  #- touch mysleep.brk
  #- >-
  #    while [ -f mysleep.brk ]; do echo "."; sleep 30; done
  - >-
      ARCH=arm make -C linux -j7 opi-h3-cam_defconfig
  - >-
      make -C linux -j7 uImage LOADADDR=0x40008000 dtbs > /dev/null
  - >- 
       make -C linux -j7  modules > /dev/null
  - make  -C linux modules_install INSTALL_MOD_PATH="${MYBOARD}/usr/"
  - make  -C linux headers_install INSTALL_HDR_PATH="${MYBOARD}/usr/" > /dev/null
  - cp -f $KBUILD_OUTPUT/.config public/linux.config
  - cp -f $KBUILD_OUTPUT/arch/arm/boot/dts/sun8i-h3-orangepi-one.dtb $MYBOARD/boot/board.dtb
  - make -C linux savedefconfig
  - cp $KBUILD_OUTPUT/defconfig public/linux-${BOARD_NAME}-defconfig
  - cp -v $KBUILD_OUTPUT/arch/arm/boot/uImage $MYBOARD/boot/uImage
  - mkimage -C none -A arm -T script -d $MYBOARD/boot/boot.cmd $MYBOARD/boot/boot.scr
  - cd $MYBOARD/boot/overlay && make
  - dtc -I dtb -O dts $MYBOARD/boot/board.dtb -o $MYBOARD/boot/board.dts
  - dtc -I dtb -O dts $MYBOARD/boot/opi-one-cam-nfc-spidev.dtb -o $MYBOARD/boot/opi-one-cam-nfc-spidev.dts
  - cp $MYBOARD/boot/opi-one-cam-nfc-spidev.dts $CWD/public/
  - cd $MYBOARD && tar -cJf $CWD/public/full-boot.tar.xz . 
  - cd $CI_PROJECT_DIR
  - echo done
  artifacts:
    paths:
    - public


Release_master:
  stage: review
  environment:
    name: master
    url: http://master.$CI_PROJECT_PATH_SLUG.$DEPLOY_DOMAIN_APP
  script:
    - deployctl release
  only:
  - master
  tags:
    - deployctl-gioxapp.com
  allow_failure: false

Release_Staging:
  stage: staging
  environment:
    name: staging
    url: |-
         http://staging.
         $CI_PROJECT_PATH_SLUG.
         $DEPLOY_DOMAIN_APP
  variables:
    GIT_STRATEGY: none
  script:
    - deployctl release
  only:
    - tags
  except:
    - branches
  tags:
    - deployctl-gioxapp.com

Release_production:
  stage: production
  variables:
    GIT_STRATEGY: none
  environment:
    name: production
    url: https://$DEPLOY_DOMAIN/$CI_BUILD_REF_SLUG
  script:
    - deployctl release
  only:
  - tags
  except:
  - branches
  tags:
    - deployctl-gioxapp.com
  when: manual
