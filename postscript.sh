#!/bin/bash
. trap_print
cd $BASE/Toolchains
ls -la
tar -xJf gcc-arm-8.2-2019.01-x86_64-arm-linux-gnueabihf.tar.xz
ls -la
mkdir -pv "${target}/opt/Toolchains"
mv -v gcc-arm-8.2-2019.01-x86_64-arm-linux-gnueabihf "${target}/opt/Toolchains/arm"
cd $BASE
echo -e "\n/opt/Toolchains/arm/lib\n" >> "${target}/etc/ld.so.conf"
/usr/sbin/ldconfig -r "${target}" -f "/etc/ld.so.conf" -C "/etc/ld.so.cache"
ls -la ${target}/opt/Toolchains/arm/bin
cp -vf /usr/sbin/trap_print $target/usr/sbin/trap_print
 