setenv load_addr "0x440000000"

# Print boot source
itest.b *0x10028 == 0x00 && echo "U-boot loaded from SD"
itest.b *0x10028 == 0x02 && echo "U-boot loaded from eMMC or secondary SD"
itest.b *0x10028 == 0x03 && echo "U-boot loaded from SPI"

echo "Boot script loaded from ${devtype}"

setenv bootargs 'root=/dev/mmcblk0p1 rw rootwait console=tty0 console=ttyS0,115200n8 rootfs=ext4 noinitrd selinux=0 splashimage=(mmc0,1)/boot/boot.bmp'

ext4load mmc 0:1 ${kernel_addr_r} /boot/uImage
ext4load mmc 0:1 ${fdt_addr_r} /boot/opi-one-cam-nfc-spidev.dtb
bootm ${kernel_addr_r} - ${fdt_addr_r}

# Recompile with:
# mkimage -C none -A arm -T script -d boot.cmd boot.scr
