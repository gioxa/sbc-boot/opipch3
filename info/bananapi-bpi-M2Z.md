## CSI Camera Connector specification:

Source: [Banana_Pi_BPI-M2_ZERO](http://wiki.banana-pi.org/Banana_Pi_BPI-M2_ZERO)

Preface:

Tests show that orangepi camera extension board with ov5640 works on banaapi P2/M2 Zero

The CSI Camera Connector is a 24-pin FPC connector which can connect external camera module with proper signal pin mappings. The pin definitions of the CSI interface are shown as below. This is marked on the Banana Pi board as “CSI″.

### 24 PIN CSI Camera connector of Banana pi BPI-M2 Zero

| CSI Pin Name | Default Function | Function2：GPIO |
|---|---|---|
|  CN3-P01 | NC | |
|  CN3-P02 | GND | |
|  CN3-P03 | CSI0-SDA | PE13 |
|  CN3-P04 | CSI0-AVDD | |
|  CN3-P05 | CSI0-SCK | PE12 |
|  CN3-P06 | CSI0-Reset | PE14 |
|  CN3-P07 | CSI0-VSYNC | PE3 |
|  CN3-P08 | CSI0-PWDN | PE15 |
|  CN3-P09 | CSI0-HSYNC | PE2 |
|  CN3-P10 | CSI0-DVDD | |
|  CN3-P11 | CSI0-DOVDD | |
|  CN3-P12 | CSI0-D7 | PE11 |
|  CN3-P13 | CSI0-MCLK | PE1 |
|  CN3-P14 | CSI0-D6 | PE10 |
|  CN3-P15 | GND | |
|  CN3-P16 | CSI0-D5 | | PE9 |
|  CN3-P17 | CSI0-PCLK | PE0 |
|  CN3-P18 | CSI0-D4 | PE8 |
|  CN3-P19 | CSI0-D0 | PE4 |
|  CN3-P20 | CSI0-D3 | PE7 |
|  CN3-P21 | CSI0-D1 | PE5 |
|  CN3-P22 | CSI0-D2 | PE6 |
|  CN3-P23 | GND | |
|  CN3-P24 | CSI0-DOVDD | |
