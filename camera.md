# dts overlay camera on CSI



## extension Board

#### csi gpio pin functions

| function | pin | GPIO dts |
|---|---|---|
| CSI_RESET# | PE14 | <0xa 0x4 0xe 0x1> |
| CSI_STBY_EN | PE15 | <0xa 0x4 0xf 0x0> |
| CSI_PWR_EN | PA17 | <0xa 0x0 0x11 0x0> |
| AFCC_EN | PG13 | <0xa 0x6 0xd 0x0> |
| CSI_EN | PG11 | <0xa 0x6 0xb 0x0> |


### power

```bash
sudo sunxi-pio -m "PG11<1><0><1><1>" #=> VCC-CSI = 2.8V CSI_EN
sudo sunxi-pio -m "PA17<1><0><1><1>" #=> VDD-CSI1/2 = 1.5V CSI_PWR_EN
sudo sunxi-pio -m "PG13<1><0><1><1>" #=> AFVCC-CSI = 2.8V AFCC_EN
```



### standby enable 

stby enable off:

```bash
sudo sunxi-pio -m "PE15<1><0><1><0>"
```

### reset

```bash
sudo sunxi-pio -m "PE14<1><0><1><0>";sudo sunxi-pio -m "PE14<1><0><1><1>"
```

### i2c check

in dts we enable i2c 0..2, results in:

```bash
i2cdetect -l
i2c-3   i2c             mv64xxx_i2c adapter                     I2C adapter
i2c-1   i2c             mv64xxx_i2c adapter                     I2C adapter
i2c-2   i2c             mv64xxx_i2c adapter                     I2C adapter
i2c-0   i2c             DesignWare HDMI                         I2C adapter
```

check i2c2, mapped by kernel as `/dev/i2c-3`

```
sudo i2cdetect -y 3
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- UU -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- --      
```

shows `UU` at our sensor adress `0x3c`

## dts overlay



```c
/dts-v1/;
/plugin/;
// plugin csi cam ov5640 for opu orangepi-one
/ {
    compatible = "xunlong,orangepi-one", "allwinner,sun80i-h3";
    
    // allow to trace overlays in /proc/device-tree/chosen/overlays
    fragment@0  {
        target-path = "/";
        __overlay__ {
                chosen{
                                overlays {
                                        SSL_CAM_OV5640 ="ODAGRUN_GIT_VERSION";
                                };
                };
                aliases{
                i2c-1 = "/soc/i2c@1c2ac00";
                i2c-2 = "/soc/i2c@1c2b000";
                i2c-3 = "/soc/i2c@1c2b400/";
                camera0= "/soc/camera@1cb0000/";
                };
        };             
    };

    /* Remove PA17 from SPDIF tx pin for CSI-PWR-EN */
    fragment@1 {
        target-path = "/soc/pinctrl@1c20800/spdif-tx-pin";
        __overlay__ {
         pins;
      };
    };

    /* camera csi*/
    fragment@2 {
        target-path = "/soc/camera@1cb0000/";
        __overlay__ {
            status = "ok";
            port {
                // Parallel bus endpoint
                csi_from_ov5640: endpoint {
                    remote-endpoint = <&ov5640_to_csi>;
                    bus-width = <8>;
                    data-shift = <2>;
                    hsync-active = <1>; // Active high
                    vsync-active = <0>; // Active low
                    data-active = <1>;  // Active high
                    pclk-sample = <1>;  // Rising
                };
            };
        };
    };

    /* add pull ups for i2c */    
    fragment@3 {
        target-path = "/soc/pinctrl@1c20800/i2c0-pins";
        __overlay__ {
         bias-pull-up;
      };
    };

    fragment@4 {
        target-path = "/soc/pinctrl@1c20800/i2c1-pins";
        __overlay__ {
         bias-pull-up;
      };
    };

    fragment@5 {
        target-path = "/soc/pinctrl@1c20800/i2c2-pins";
        __overlay__ {
         bias-pull-up;
      };
    };
  
   /* regulators for camera */
  
    fragment@6 {
        target-path = "/soc/";
        __overlay__ {    

        reg_cam_vcc: cam-vcc {
            compatible = "regulator-fixed";
            regulator-name = "opi-eb-500m-vcc";
            regulator-min-microvolt = <2800000>;
            regulator-max-microvolt = <2800000>;
            regulator-boot-on;
            enable-active-high;
            gpio = <0xa 0x6 0xb 0x0>; // <&pio 6 11 GPIO_ACTIVE_HIGH>;  PG11 CSI_EN
        };
        
        reg_cam_vdd: cam-vdd {
            compatible = "regulator-fixed";
            regulator-name = "opi-eb-500m-vdd";
            regulator-min-microvolt = <1500000>;
            regulator-max-microvolt = <1500000>;
            regulator-boot-on;
            enable-active-high;
            gpio = <0xa 0x0 0x11 0x0>; // <&pio 0 17 GPIO_ACTIVE_HIGH>;  PA17 CSI_PWR_EN
        };
 
        reg_cam_af_vcc: cam-af-vcc {
            compatible = "regulator-fixed";
            regulator-name = "opi-eb-500m-af-vcc";
            regulator-min-microvolt = <2800000>;
            regulator-max-microvolt = <2800000>;
            enable-active-high;
            regulator-boot-on;
            gpio = <0xa 0x6 0xd 0x0>; // <&pio 6 13 GPIO_ACTIVE_HIGH>;  PG13 AFCC_EN 
        };
      }; // /overlay
   }; // /fragment@6

   // enable all i2c
   fragment@7 {
                target-path = "/soc/i2c@1c2ac00";
                __overlay__ {
                        status = "okay";
                };
        };

   fragment@8 {
                target-path = "/soc/i2c@1c2b000";
                __overlay__ {
                        status = "okay";
                };
        };

    /* update i2c2 with ov5640 camera */
    fragment@9 {
        target-path = "/soc/i2c@1c2b400/";
        __overlay__ {
            status= "okay";
            clock-frequency = <400000>; // i2c freqentie 400k
            #address-cells = <1>;
            #size-cells = <0>;
            ov5640: camera@3c {
                compatible = "ovti,ov5640";
                reg = <0x3c>;
                clocks = <0x3 0x6b>; // <&ccu CLK_CSI_MCLK> sun8-h3-ccu.h #define CLK_CSI_MCLK          107
                clock-names = "xclk";
                AVDD-supply = <&reg_cam_vcc>;
                DOVDD-supply = <&reg_cam_vcc>;
                DVDD-supply = <&reg_cam_vdd>;
                AFVDD-supply= <&reg_cam_af_vcc>;
                reset-gpios = <0xa 0x4 0xe 0x1>; //<&pio 4 14 GPIO_ACTIVE_LOW>; /* PE14 => CSI-RESET#*/
                powerdown-gpios = <0xa 0x4 0xf 0x0>; ///* <&pio 4 15 GPIO_ACTIVE_HIGH> PE15 CSI-STDB_EN */
                port {
                    /* i2c bus endpoint */
                    ov5640_to_csi: endpoint {
                        remote-endpoint = <&csi_from_ov5640>;
                        bus-width = <8>; // we use D2..D10 : 8 bits
                        data-shift = <2>; // we shift 2, since we start at D2 iso D0
                        hsync-active = <1>;
                        vsync-active = <0>;
                        data-active = <1>;
                        pclk-sample = <1>;
                    };
                };
            };
        };
    };

/*
  Add CSI-MCLK as CSI function=> "PE1" missing from mainline dtsi
  cam500A/B have an 24Mhz oscilator on the extension board , 
  the orangepi camera extension board _200M&500M has not 
  and does need to have this pin enabled for the OV-5640 sensor to have a master clock.
*/
    fragment@10 {
        target-path = "/soc/pinctrl@1c20800/csi-pins";
        __overlay__ {
         pins = "PE0", "PE1", "PE2", "PE3", "PE4", "PE5", "PE6", "PE7", "PE8", "PE9", "PE10", "PE11";
      };
    };
};
```

compile `SSL_CAM_OV5640.dts` with:

```bash
dtc -O dtb -I dts -o SSL_CAM_OV5640.dtbo -b 0  SSL_CAM_OV5640.dts
```

check new dtbo:

```bash

fdtdump SSL_CAM_OV5640.dtbo

```

create new board dtb:

```bash
fdtoverlay -i /boot/board.dtb -o /boot/bwSSL_CAM.dtb -v /boot/overlay/SSL_CAM_OV5640.dtbo
```

## boot

`boot.cmd`

```bash
setenv load_addr "0x40000000"

# Print boot source
itest.b *0x10028 == 0x00 && echo "U-boot loaded from SD"
itest.b *0x10028 == 0x02 && echo "U-boot loaded from eMMC or secondary SD"
itest.b *0x10028 == 0x03 && echo "U-boot loaded from SPI"

echo "Boot script loaded from ${devtype}"

setenv bootargs 'root=/dev/mmcblk0p1 rw rootwait console=tty0 console=ttyS0,115200n8 rootfs=ext4 noinitrd selinux=0 splashimage=(mmc0,1)/boot/boot.bmp'

ext4load mmc 0:1 ${kernel_addr_r} /boot/uImage
ext4load mmc 0:1 ${fdt_addr_r} /boot/bwSSL_CAM.dtb
bootm ${kernel_addr_r} - ${fdt_addr_r}

# Recompile with:
# mkimage -C none -A arm -T script -d boot.cmd boot.scr
```


list flat tree:

```bash
fdtdump /boot/bwSSL_CAM.dtb
```

list from running device:

```bash
dtc -I fs /proc/device-tree
```

check our chosen overlay version:

```
cat /proc/device-tree/chosen/overlays/SSL_CAM_OV5640
```

### TODO:

check dts with:

```diff
 fragment@6 {
        target-path = "/soc/";
        __overlay__ {    
+        cam_xclk: cam-xclk {
+            #clock-cells = <0>;
+            compatible = "fixed-clock";
+            clock-frequency = <24000000>;
+            clock-output-names = "cam-xclk";
+        };

```
and for i2c2 overlay

```diff
          ov5640: camera@3c {
                compatible = "ovti,ov5640";
                reg = <0x3c>;
-                clocks = <0x3 0x6b>; // <&ccu CLK_CSI_MCLK> sun8-h3-ccu.h #define CLK_CSI_MCLK          107
+                clocks = <&cam_xclk>;

```
              
## display video




Display 720P (1280x720) on 1080P monitor full screen:

```bash
media-ctl -d /dev/media1 --set-v4l2='5:0 [fmt:UYVY8_2X8/1280x720@1/30 field:none colorspace:srgb xfer:srgb ycbcr:601 quantization:full-range]'
```

```bash
media-ctl -d /dev/media1 -p
Media controller API version 5.3.0

Media device information
------------------------
driver          sun6i-csi
model           Allwinner Video Capture Device
serial          
bus info        
hw revision     0x0
driver version  5.3.0

Device topology
- entity 1: sun6i-csi (1 pad, 1 link)
            type Node subtype V4L flags 0
            device node name /dev/video0
        pad0: Sink
                <- "ov5640 3-003c":0 [ENABLED,IMMUTABLE]

- entity 5: ov5640 3-003c (1 pad, 1 link)
            type V4L2 subdev subtype Sensor flags 0
            device node name /dev/v4l-subdev0
        pad0: Source
                [fmt:UYVY8_2X8/1280x720@1/30 field:none colorspace:srgb xfer:srgb ycbcr:601 quantization:full-range]
                -> "sun6i-csi":0 [ENABLED,IMMUTABLE]
```


```
mplayer -vo sdl:driver=fbcon -slave -tv driver=v4l2:width=1280:height=720:device=/dev/video0 tv:// -vm -nosound  -x 1920 -y 1080 -fs -quiet
```




```bash
mplayer -fs tv:// -tv driver=v4l:width=352:height=288
```
or

```bash
mplayer tv:// -tv driver=v4l2:width=640:height=480:device=/dev/video0:fps=30:outfmt=yuy2
```

```
cvlc --no-audio v4l2:///dev/video0:width=640:height=480
```

FROM: [linux questions: adjusting-v4l2-settings-after-launching-mplayer-to-framebuffer-in-console](https://www.linuxquestions.org/questions/linux-software-2/adjusting-v4l2-settings-after-launching-mplayer-to-framebuffer-in-console-4175493692-print/)

stream with `mplayer -tv driver=v4l2:device=/dev/video0 tv://` and mplayer resets the v4l2 settings e.g. `v4l2-ctl --set-ctrl brightness=134`

Mplayer can be set into a slave mode where you can feed it more commands. Among those commands is the "run" command which can run a terminal command enclosed in quotation marks. You can also direct commands into mplayer from a file. Combined, these two features lead to a solution:

create a file with the mplayer commands you wish to give. My file is called "v4l-mplayer-settings". The file consists of the following:
Code:
```bash
run "v4l2-ctl --set-ctrl contrast=43"
run "v4l2-ctl --set-ctrl saturation=38"
```

Then I run this command in the terminal (or from a bash script):
Code:
```bash
mplayer -vo sdl:driver=fbcon -slave -quiet -input file=v4l-mplayer-settings -tv driver=v4l2:device=/dev/video0 tv://
```
the `-vo` flag and the following `sdl:driver=fbcon` may be particular to my system, as a lot of people suggest using `fbdev` instead of `fbcon`. This sends your video to the framebuffer. It may be optional to set this if you are running the mplayer command from the console without any x software running.

`-slave` puts mplayer in slave mode to accept commands
`-quiet` is meant to provide you an mplayer-specific command line where you can feed mplayer commands via the keyboard. I'm not sure if it's required if you use `-input` file. However, it doesn't hurt.
`-input file`= is where I direct mplayer to use the commands I included in the above v4l-mplayer-settings file.
`-tv` indicates that we are setting properties for the tv device, using video for linux 2 (which is what I need to use to access my webcam), and the device at /dev/video0 (which is the video device of my webcam - your system may be different.
Finally, `tv://` just directs mplayer to invoke its TV viewer system.

## use v4l2 api

[Capture images using V4L2 on Linux](https://jayrambhia.com/blog/capture-v4l2) source [capturev4l2.c](https://gist.github.com/jayrambhia/5866483)

## more info mediactl and v4l2 ctl

http://trac.gateworks.com/wiki/linux/media

## 1080p with ov5640, 15 f/s

```
media-ctl -d /dev/media1 --set-v4l2='5:0 [fmt:UYVY8_2X8/1920x1080@1/15 field:none colorspace:srgb xfer:srgb ycbcr:601 quantization:full-range]'
mplayer -vo sdl:driver=fbcon -slave -quiet -tv driver=v4l2:width=1920:height=1080:device=/dev/video1:fps=15:outfmt=uyvy tv:// 
```
